var defaults = {
  isEmailNotificationEnabled: false,
  isPushNotificationEnabled: false
};

module.exports = defaults;
