var express = require("express");
var router = express.Router();
var oneSignalConfig = require("../config/OneSignal.json");
var OneSignal = require("onesignal-node");
const stringify = require("json-stringify");

var notification = require("../helpers/notificationHelper");
var forwarder = require("../helpers/forwarderHelper");
//var onesignal = require("../helpers/OneSignal");

//One Signal functional
function OneSignalNotification(user_id, message, extra_data, title) {
  var myClient = new OneSignal.Client({
    userAuthKey: oneSignalConfig.userAuthKey,
    app: {
      appAuthKey: oneSignalConfig.appAuthKey,
      appId: oneSignalConfig.appId
    }
  });

  var firstNotification = new OneSignal.Notification({
    headings: {
      en: title
    },
    contents: {
      en: message
    },
    data: {
      task_id: extra_data
    },
    //include_external_user_ids: [user_id]
    include_external_user_ids: [user_id]
  });

  myClient.sendNotification(firstNotification, function(
    err,
    httpResponse,
    data
  ) {
    if (err) {
      console.log("Something went wrong...");
    } else {
      console.log(data);
      return data;
    }

  });
}

router
  .route("/referralNotification")
  .post(async function(req, res) {
    //  let response = await notification.sendN(req);
    try {
      //console.log("task lambda url ->" + process.env.task_lambda);
      /*let response = await forwarder.forwardRequest(
        "POST",
        process.env.task_lambda,
        "/task/application",
        req.body,
        req.headers
      );
      console.log("Post to application response body taskInstanceID -> " + req.body.taskInstanceID);
      //res.json(stringify(response));
      //get task details by task instance id
      let taskData = await forwarder.forwardRequest(
        "GET",
        process.env.task_lambda,
        //"localhost:7001",
        "/task/instance/getByID/" + req.body.taskInstanceID,
        "",
        req.headers
      );*/
      //console.log("Task Data" + JSON.stringify(taskData.body));
      //taskData = taskData.body[0];
      //taskData = JSON.stringify(taskData.body);
      //console.log("Salon ID " +taskData.salonID+" task name " + taskData.taskName +" Task ID is "+req.body.taskInstanceID);
      let salonParam = {
        salonID: req.body.salon_id,
        notification_type:req.body.notification_type,
        referee_name:req.body.referee_name
      };
     console.log("Salon param",salonParam);
      switch (req.body.notification_type) {
      case 16:
      {
        receiver_id = salonParam.salonID;
        salonParameters = {
          referee_name: salonParam.referee_name
        };
        notification_type = "SUCCESSFUL_REFERRAL";
console.log("Receiver id",receiver_id);
        //One signal notification call
        let os = await OneSignalNotification(
          receiver_id,
          salonParameters.referee_name +
            " has succesfully used your referral code. You have two free task added",
      null,
          "Successful Referral"
        );
      //  res.json(stringify(os));
      }
      break;
      case 17:
      {
        receiver_id = salonParam.salonID;
        salonParameters = {
          referee_name: salonParam.referee_name
        };
        notification_type = "REFEREE_SUBSCRIBED";

        //One signal notification call
        let os = await OneSignalNotification(
          receiver_id,
          salonParameters.referee_name +
            ", who had used your referral code has succesfully bought a subscription. You have 10 free tasks added",
      null,
      salonParameters.referee_name +" bought a Subscription!"
        );
      //  res.json(stringify(os));
      }
      break;
      case 18:
      {
        receiver_id = salonParam.salonID;
        salonParameters = {
          referee_name: salonParam.referee_name
        };
        notification_type = "REFEREE_TASKS_COMPLETE";

        //One signal notification call
        let os = await OneSignalNotification(
          receiver_id,
          salonParameters.referee_name +
            ", who had used your referral code  has succesfully completed 5 appointments. You have 5 free appointments added",
      null,
          salonParameters.referee_name +" completed 5 appointments"
        );
      //  res.json(stringify(os));
      }
      break;
    default:{console.log("In default")}
  }


  //send notification to salon using task info.
  let notificationResponse = await notification.sendN(
    0,
    receiver_id,
    notification_type,
    salonParam
  );
  res.json({
    notificationResponse
  });
  } catch (err) {
  console.log(err);
  res.json(err);
  }
  });

  module.exports = router;
