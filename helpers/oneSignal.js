var oneSignalConfig = require("../config/OneSignal.json");
var OneSignal = require('onesignal-node');

function OneSignalNotification(user_id, message, extra_data, title){
var myClient = new OneSignal.Client({
    userAuthKey: oneSignalConfig.userAuthKey,
    app: { appAuthKey: oneSignalConfig.appAuthKey, appId: oneSignalConfig.appId }
});

var firstNotification = new OneSignal.Notification({
  headings: {
      en: title
  },
    contents: {
        en: message
    },
    data:{
      data:extra_data
    },
    //include_external_user_ids: [user_id]
    include_external_user_ids: [user_id]
});


myClient.sendNotification(firstNotification, function (err, httpResponse,data) {
   if (err) {
       console.log('Something went wrong...');
   } else {
       console.log(data);
}
});
return data;
}
