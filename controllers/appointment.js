var express = require("express");
var router = express.Router();
var oneSignalConfig = require("../config/OneSignal.json");
var OneSignal = require("onesignal-node");
const stringify = require("json-stringify");

var notification = require("../helpers/notificationHelper");
var forwarder = require("../helpers/forwarderHelper");
//var onesignal = require("../helpers/OneSignal");

//One Signal functional
function OneSignalNotification(user_id, message, extra_data, title) {
  var myClient = new OneSignal.Client({
    userAuthKey: oneSignalConfig.userAuthKey,
    app: {
      appAuthKey: oneSignalConfig.appAuthKey,
      appId: oneSignalConfig.appId
    }
  });

  var firstNotification = new OneSignal.Notification({
    headings: {
      en: title
    },
    contents: {
      en: message
    },
    data: {
      task_id: extra_data
    },
    //include_external_user_ids: [user_id]
    include_external_user_ids: [user_id]
  });

  myClient.sendNotification(firstNotification, function(
    err,
    httpResponse,
    data
  ) {
    if (err) {
      console.log("Something went wrong...");
    } else {
      console.log(data);
      return data;
    }

  });
}

//End of OneSignal call

router
  .route("/appointmentPOST")

  //extra things for task application post
  .post(async function(req, res) {
    //  let response = await notification.sendN(req);
    try {
      //console.log("task lambda url ->" + process.env.task_lambda);
      
      //console.log("Post to application response body taskInstanceID -> " + req.body.taskInstanceID);
      //res.json(stringify(response));
      //get task details by task instance id
      // let taskData = await forwarder.forwardRequest(
      //   "GET",
      //   process.env.appointment_lambda,
      //   //"localhost:7001",
      //   "/appointment/getByID/" + req.body.taskInstanceID,
      //   "",
      //   req.headers
      // );
      //console.log("Task Data" + JSON.stringify(taskData.body));
      //taskData = taskData.body[0];
      taskData = req.body;
     // console.log("Salon ID " +taskData.salonID+" appointment ID " + taskData1. +" Task ID is "+req.body.taskInstanceID);
      let taskParam = {
         customerName: taskData.customerName,
         appointmentID: req.body.id,
         salonName: req.body.salonName,
         serviceName:req.body.serviceName
       };


      let os = await OneSignalNotification(
      //  OneSignalNotification(
        taskData.salonID,
        req.body.customerName +
          " has booked for an appointment " +
          taskData.taskName +
          ". Kindly review and make arrangements accordingly.",
        taskParam.appointmentID,
        "New Booking confirmed"
      );
      console.log("Notification params ->", taskParam);
      console.log("Taskdata",taskData);
      //send notification to salon using task info.
      let notificationResponse = await notification.sendN(
        taskParam.appointmentID,
        taskData.salonID,
        //"2",
        "SUCCESSFUL_BOOKING",
        taskParam
      );
      console.log("Notification response ->" + notificationResponse);
      //console.log("Salon ID" +taskData.salonID+"task name" + taskData.taskName+" APplicant name is "+req.body.applicantName+" Task ID is "+taskParam.taskID);

      res.json(
        stringify({
          os,
          notificationResponse
        })
      );

      //One signal notification call

    //  res.json(stringify(os));
    } catch (err) {
      console.log(err);
      res.json(err);
    }
  });
router
  .route("/appointment/status/:id")
  .put(async function(req, res) {
    //  let response = await notification.sendN(req);
    try {
      let response = await forwarder.forwardRequest(
        "PUT",
        //"https://652ngrocf0.execute-api.ap-south-1.amazonaws.com/default",
        process.env.task_lambda,
        "/appointment/data/updateByID/" + req.params.id,
        req.body
        //req.headers
      );
      //  res.json(response);

      //get task details by task instance id
      let appData = await forwarder.forwardRequest(
        "GET",
        //"https://652ngrocf0.execute-api.ap-south-1.amazonaws.com/default",
        process.env.task_lambda,
        "/appointment/data/getByID/" + req.params.id,
        ""
        //req.headers
      );

      appData = appData.body[0];

      let taskParam = "",
        receiver_id = "",
        notification_type = "";
      //condition 1 CONFIRMED_FOR_TASK
      switch (req.body.appointmentStatus) {
        case 6:
          {
            //console.log("APplicaion status------>"+req.body.applicationStatus);
            receiver_id = appData.salonID;
            taskParam = {
              customerName: appData.customerName,
              serviceName: req.query.serviceName,
              salonName: req.query.salonName,
              appointmentID: req.params.id
            };
            //console.log("Task param---->",taskParam);
            notification_type = "APPOINTMENT_CANCELLED_SALON";

            //One signal notification call
            let os = await OneSignalNotification(
              receiver_id,
                " Customer "+ taskParam.customerName + "has cancelled the booking. Kindly review and confirm accordingly.",
              taskParam.appointmentID,
              "Booking Cancelled by Customer"
            );
            //res.json(stringify(os));
          }
          break;
        case 1:
          {
            //console.log("APplicaion status------>"+req.body.applicationStatus);
            receiver_id = appData.userID;
            taskParam = {
              customerName: appData.customerName,
              serviceName: req.query.serviceName,
              salonName: req.query.salonName,
              appointmentID: req.params.id
            };
            //console.log("Task param---->",taskParam);
            notification_type = "APPOINTMENT_CANCELLED_CUSTOMER";

            //One signal notification call
            let os = await OneSignalNotification(
              receiver_id,
                " Salon "+ taskParam.salonName + "has cancelled the booking. Please book another appointment at another salon or timeslot.",
              taskParam.appointmentID,
              "Booking Cancelled by Salon"
            );
            //res.json(stringify(os));
          }
          break;
        case 5:
          {
            //console.log("APplicaion status------>"+req.body.applicationStatus);
            receiver_id = appData.userID;
            taskParam = {
              customerName: appData.customerName,
              serviceName: req.query.serviceName,
              salonName: req.query.salonName,
              appointmentID: req.params.id
            };
            //console.log("Task param---->",taskParam);
            notification_type = "APPOINTMENT_COMPLETE";

            //One signal notification call
            let os = await OneSignalNotification(
              appData.customerID,
                " Your appointment for " +taskParam.ServiceName+ "at " +taskParam.salonName+ "has been completed. Please review.",
              taskParam.appointmentID,
              "Appointment Complete"
            );
            //res.json(stringify(os));
          }
          break;
        case 8:
          {
            //console.log("APplicaion status------>"+req.body.applicationStatus);
            receiver_id = appData.userID;
            taskParam = {
              customerName: appData.customerName,
              serviceName: req.query.serviceName,
              salonName: req.query.salonName,
              appointmentID: req.params.id
            };
            //console.log("Task param---->",taskParam);
            notification_type = "APPOINTMENT_NOSHOW_CUSTOMER";

            //One signal notification call
            let os = await OneSignalNotification(
              receiver_id,
              "Your appointment for " +taskParam.ServiceName+ "at " +taskParam.salonName+ "was not completed or you did not show up. Please review.",
              taskParam.appointmentID,
              "No Show by Customer"
            );
            //res.json(stringify(os));
          }
          break;
        
        default:{console.log("In default")}
      }

      //send notification to salon using task info.
      let notificationResponse = await notification.sendN(
        req.params.id,
        receiver_id,
        notification_type,
        taskParam
      );
      res.json({
        response,
        appData,
        notificationResponse
      });
    } catch (err) {
      console.log(err);
      res.json(err);
    }
  });

module.exports = router;
