var express = require("express");
var router = express.Router();
var oneSignalConfig = require("../config/OneSignal.json");
var OneSignal = require("onesignal-node");
const stringify = require("json-stringify");

var notification = require("../helpers/notificationHelper");
var forwarder = require("../helpers/forwarderHelper");


//One Signal functional
function OneSignalNotification(user_id, message, extra_data, title) {
  var myClient = new OneSignal.Client({
    userAuthKey: oneSignalConfig.userAuthKey,
    app: {
      appAuthKey: oneSignalConfig.appAuthKey,
      appId: oneSignalConfig.appId
    }
  });

  var firstNotification = new OneSignal.Notification({
    headings: {
      en: title
    },
    contents: {
      en: message
    },
    data: {
      task_id: extra_data
    },
    include_external_user_ids: [user_id]
  });

  myClient.sendNotification(firstNotification, function(
    err,
    httpResponse,
    data
  ) {
    if (err) {
      console.log("Something went wrong...");
    } else {
      console.log(data);
      return data;
    }

  });
}

router
  .route("/kycapplication")
  .post(async function(req, res) {

    try {
      let salonParam = {
        userID: req.body.user_id,
        notification_type:req.body.notification_type,
      };
     console.log("Salon param",salonParam);
      switch (req.body.notification_type) {
      case 20:
      {
        receiver_id = salonParam.userID;
        notification_type = "SALON_KYC_FAILURE";
        console.log("Receiver id",receiver_id);
        //One signal notification call
        let os = await OneSignalNotification(
          receiver_id,
          "Your KYC has been declined. Please resubmit correct data",
      null,
          "KYC application declined"
        );
      //  res.json(stringify(os));
      }
      break;
      case 19:
      {
        receiver_id = salonParam.userID;
        notification_type = "SALON_KYC_SUCCESS";
console.log("Receiver id",receiver_id);
        //One signal notification call
        let os = await OneSignalNotification(
          receiver_id,
          "Your KYC has been approved. Please start creating or accepting tasks",
          null,
          "KYC Approved"
        );
      //  res.json(stringify(os));
      }
      break;
    default:{console.log("In default")}
  }


  //send notification to salon using task info.
  let notificationResponse = await notification.sendN(
    0,
    receiver_id,
    notification_type,
    salonParam
  );
  res.json({
    notificationResponse
  });
  } catch (err) {
  console.log(err);
  res.json(err);
  }
  });

  module.exports = router;
