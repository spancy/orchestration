var mailer = require("nodemailer-promise");

//var emailConfig = require("../config/emailConfig");
var functionalConfig = require("../config/functionalConfig");
var notificationConfig = require("../config/notificationConfig");
var forwarder = require("../helpers/forwarderHelper");
var moment = require("moment");
module.exports.sendN = async function sendN(senderID,receiverID,type,nParams,headers) {

  var getMessage =
    {
    sender_id: senderID,
    receiver_id: receiverID,
    appointment_id: senderID,
    createdTimeAtSource: Date.now() ,
    type: type,
    title: notificationConfig[type].title.apply(nParams),
    message: notificationConfig[type].message.apply(nParams),
    };

  
  try {
    let response = await forwarder.forwardRequest(
      "POST",
      process.env.ns_lambda,
      //"https://4ce3se7o0i.execute-api.ap-south-1.amazonaws.com/default",
      "/notification/data",
      getMessage,
      headers
    );
    return response;
    //return `Successfully processed notification for ${type} with params ${nParams}`;
  } catch (err) {
    console.log(err);
    return "Notification errored for ${type} with params ${nParams}";
  }
};
// module.exports.sendEmail = function(subject, receiver, html) {
//   var sendEmail = mailer.config({
//     email: emailConfig.senderMail,
//     password: emailConfig.senderMailPassword,
//     server: emailConfig.senderServer
//   });
//   var options = {
//     subject: subject,
//     senderName: emailConfig.senderName,
//     receiver: receiver,
//     html: html
//   };

//   return sendEmail(options);
//};





