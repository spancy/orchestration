var defaults = {
  CUSTOMER_KYC_COMPLETE: {
    title: function() {
      return "Complete your KYC";
    },

    message: function() {
      return "Complete your KYC";
    }
  },
  SALON_KYC_COMPLETE: {
    title: function() {
      return "Complete your KYC";
    },

    message: function() {
      return "Complete your KYC";
    }
  },
  BOOKING_INITIATED: {
    title: function() {
      return `New Booking for ${this.serviceName} initiated.`;
    },

    message: function() {
      return `You have initiated a booking for a ${
        this.serviceName
    }. Kindly review and confirm the booking accordingly.`;
    }
  },
  SUCCESSFUL_BOOKING: {
    title: function() {
      return `Booking confirmation.`;
    },

    message: function() {
      return  `Booking for ${this.serviceName} has been completed. Please check your booking details and confirmation.`;
    }
  },
  PAYMENT_COMPLETE: {
    title: function() {
      return `Payment confirmation.`;
    },

    message: function() {
      return  `Payment for ${this.serviceName} has been completed. Please check the booking details and confirmation.`;
    }
  },
  PAYMENT_FAILED: {
    title: function() {
      return `Booking confirmation.`;
    },

    message: function() {
      return  `Booking for ${this.serviceName} has been completed. Please check your booking details and confirmation.`;
    }
  },
  FAILED_BOOKING: {
    title: function() {
      return `Booking failed..`;
    },

    message: function() {
      return  `Booking for ${this.serviceName} has failed. Please rebook.`;
    }
  },
  APPOINTMENT_COMPLETE: {
    title: function() {
      return `Appointment Complete`;
    },

    message: function() {
      return `Your appointment for ${this.serviceName} at ${this.salonName} has been completed. Please review. `;
    }
  },
  APPOINTMENT_CANCELLED_SALON: {
    title: function() {
      return `Booking cancelled`;
    },

    message: function() {
      return `${this.customerName} has cancelled the booking for ${this.serviceName} `;
    }
  },
  APPOINTMENT_CANCELLED_CUSTOMER: {
    title: function() {
      return `Booking cancelled`;
    },

    message: function() {
      return `${this.salonName} has cancelled the booking for ${this.serviceName}. Please book another appointment at another salon or timeslot. `;
    }
  },
  APPOINTMENT_NOSHOW_CUSTOMER: {
    title: function() {
      return `Salon flagged this as a no show`;
    },

    message: function() {
      return `${this.salonName} has mentioned that you did not show up for this ${this.serviceName}. Please book another appointment at another salon or timeslot or confirm to us that you completed the booking `;
    }
  },
  SUCCESSFUL_REFERRAL: {
    title: function() {
      return `Succesful Referral`;
    },

    message: function() {
      return `${this.referee_name} has succesfully used your referral code. You have one free task added`;
    }
  },
  REFEREE_SUBSCRIBED: {
    title: function() {
      return `${this.referee_name} bought a Subscription`;
    },

    message: function() {
      return `${this.referee_name} , who had used your referral code has succesfully bought a subscription. You have 10 free tasks added`;
    }
  },
  REFEREE_TASKS_COMPLETE: {
    title: function() {
      return `${this.referee_name}  completed 5 tasks`;
    },

    message: function() {
      return `${this.referee_name}
        , who had used your referral code  has succesfully completed 5 tasks. You have 5 free tasks added`;
    }
  },

  SALON_KYC_FAILURE: { title: function() {
    return `KYC application declined`;
  },

  message: function() {
    return `Your KYC has been declined. Please resubmit correct data`;
  } },
  SALON_PAYMENT_SUCCESS: { title: "Complete your KYC" },
  SALON_PAYMENT_FAILURE: { title: "Complete your KYC" },
  EMPLOYEE_PAYMENT_SUCCESS: { title: "Complete your KYC" },
  EMPLOYEE_PAYMENT_FAILURE: { title: "Complete your KYC" }
};

module.exports = defaults;
