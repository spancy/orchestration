//Get the URL of the target lambda and call respective api. get the response and send the notification
const axios = require("axios");

async function forwardRequest(method, hostname, path, payload, headers) {
  try {
    
    var options = {
      method: method,
      url: hostname + path,
      data: payload
      //headers: { authorization: headers.authorization }
    };
    console.log("Options",options);
    const response = await axios(options);
    console.log(response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}

module.exports.forwardRequest = forwardRequest;
